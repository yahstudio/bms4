import json
import time
import requests

with open('user_id.js', 'r') as file_ptr:
	lines=file_ptr.readlines();

file_client_ptr = open("client_id.js", "w")
file_debtor_ptr = open("debtor_id.js", "w")

file_client_ptr.write('var user_id = [ \n')
file_debtor_ptr.write('var user_id = [ \n')
for line in lines:
	line = (line.split(','))[0]
	page = requests.get('https://intranet.barclaymis.com.au/v1/client/'+str(line))
	time.sleep(0.5)
	if page.text:
		try:
			response= json.loads(page.text)
			print(line,' ----->>> ',response['cat_id'])
			if response['cat_id'] == '100':
				file_client_ptr.write(str(line)+',\n')
			if response['cat_id'] == '101':
				file_debtor_ptr.write(str(line)+',\n')
		except Exception as e:
			print('error is : ',e)
			raise e

file_client_ptr.write(']')
file_debtor_ptr.write(']')

file_client_ptr.close()
file_debtor_ptr.close()