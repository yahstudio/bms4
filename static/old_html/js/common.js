// $('input:checkbox').parent().hide();
var validationArray = {
  client_code_val: /^[0-9]{1,15}$/,
  client_id_val: /^[0-9]{1,15}$/,
  abn_val: /^[0-9a-zA-Z ]{3,30}$/,
  acn_val: /^[0-9a-zA-Z ]{3,30}$/,
  invoice_type_val: /^[0-1]{1}$/,
  invoice_email: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  address_1_val: /^[#.0-9a-zA-Z\s,-]{5,100}$/,
  address_2_val: /^[#.0-9a-zA-Z\s,-]{5,100}$/,
  post_code_val: /^[0-9]{3,6}$/,

  // prop_man_title_val:/(?:^|\W)(?:(?:Dr|Mr|Mrs|Ms|Sr|Jr)\.?|Miss|Phd|\+|&)(?:\W|$)/i,
  prop_man_title_val: /^[a-zA-Z ]{2,10}$/,
  prop_man_first_val: /^[a-zA-Z ]{3,30}$/,
  prop_man_last_val: /^[a-zA-Z ]{3,30}$/,
  phone_1_val: /^[0-9 ]{5,15}$/,
  phone_2_val: /^[0-9 ]{5,15}$/,
  mobile_val: /^[0-9 ]{5,15}$/,
  mobile_2_val: /^[0-9 ]{5,15}$/,
  fax_val: /^[0-9 ]{5,15}$/,
  c_title_val: /^[0-9a-zA-Z ]{3,30}$/,


  name_val: /^[a-zA-Z ]{3,30}$/,
  title_note_val: /^[0-9a-zA-Z ]{3,30}$/,
  // real_group_val: /^[0-9a-zA-Z ]{0,30}$/,
  // login_type_val: /^[0-1]{1}$/,
  sms_name_val: /^[0-9a-zA-Z ]{3,30}$/,
  street_val: /^[0-9a-zA-Z ]{3,30}$/,
  state_val: /^[0-9]{1,15}$/,
  suburb_val:/^[0-9a-zA-Z ]{3,30}$/,
  //country_val:/^[a-zA-Z ]{3,30}$/,

  contact_1_title: /^[a-zA-Z ]{3,30}$/,
  contact_1_first: /^[a-zA-Z ]{3,30}$/,
  contact_1_last: /^[a-zA-Z ]{3,30}$/,
  contact_2_title_val: /^[a-zA-Z ]{3,30}$/,
  contact_2_first_val: /^[a-zA-Z ]{3,30}$/,
  contact_2_last_val: /^[a-zA-Z ]{3,30}$/,


  arrears_phone_val: /^[0-9 ]{5,30}$/,
  invoice_contact_val: /^[0-9 ]{5,30}$/,

  email_val: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  principal_email_val: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  prop_man_email_val: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  accounts_email_val: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  pdf_email_val: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  bdm_email_val: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,


  user_name_val: /^[0-9 ]{3,15}$/,
  password_val: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$/,
  num_props_val: /^[0-9]{0,3}$/,
  //props_as_at_val:,
  //renewal_date_val:,

  cnote_year_val: /^[0-9a-zA-Z ]{3,30}$/,
  status_val: /^[0-1]{1}$/,
  car_rego_val: /^[0-9]{5,20}$/,
  //referral_val: /^[0-1]{1}$/,
  // platinum_partner_val: /^[0-1]{1}$/,
  // no_lapse_val: /^[0-1]{1}$/,


  //debtor form
  WORKADDRESS_val: /^[#.0-9a-zA-Z\s,-]{5,100}$/,
  service_address_val: /^[#.0-9a-zA-Z\s,-]{5,100}$/,
  other_contact_val: /^[0-9]{5,10}$/,
  drive_lic_num_val: /^[0-9a-zA-Z ]{3,30}$/,
  passport_val: /^[0-9a-zA-Z ]{3,30}$/,
  //interested_locating_val :/^[0-1]{1}$/,
  managing_agent_val: /^[0-9a-zA-Z ]{3,30}$/,
  description_val: /^[0-9a-zA-Z ]{3,30}$/,
  sex_val: /^[0-9a-zA-Z ]{3,30}$/,
  DEFRELEST_val: /^[0-9a-zA-Z ]{3,30}$/,

  AMNTOFDEF_val: /^[0-9]\d*(\.\d+)?$/,
  paid_val: /^[0-9]{1,10}$/,
  stillowing_val: /^[0-9]{1,10}$/,

  // WORKADDRESS_val:/^[0-9a-zA-Z ]{3,30}$/ ,
  WorkPosition_val: /^[0-9a-zA-Z ]{3,30}$/,
  WorkHours_val: /^[0-9a-zA-Z ]{3,30}$/,

}

var validationmessage = {
  client_id_mess: "<p class='client_codecls rotate-90' style='color:red;'>please enter digit only</p>",
  client_code_mess: "<p class='client_codecls rotate-90' style='color:red;'>please enter digit only</p>",
  name_mess: "<p class='namecls rotate-90' style='color:red;'>please enter character only</p>",
  title_note_mess: "<p class=' rotate-90' style='color:red;'>Please enter digit/character only </p>",
  real_group_mess: "<p class=' rotate-90' style='color:red;'>Please enter digit only </p>",
  login_type_mess: "<p class=' rotate-90' style='color:red;'>please on/off bit </p>",
  sms_name_mess: "<p class='sms_namecls rotate-90' style='color:red;'>please enter digit/character only</p>",
  address_1_mess: "<p class='address_1cls rotate-90' style='color:red;'>please enter valid format</p>",
  address_2_mess: "<p class='address_1cls rotate-90' style='color:red;'>please enter valid format</p>",
  street_mess: "<p class='street1cls rotate-90' style='color:red;'>please enter digit/character only</p>",
  suburb_mess: "<p class='suburbcls rotate-90' style='color:red;'>please enter digit/character only</p>",
  state_mess: "<p class='statecls rotate-90' style='color:red;'>please enter digit only</p>",
  post_code_mess: "<p class='post_codecls rotate-90' style='color:red;'>please enter digit/character only</p>",
  //country_mess:"<p class='countrycls rotate-90' style='color:red;'>please enter character only</p>",
  contact_1_title_mess: "<p class='contact_2_titlecls rotate-90' style='color:red;'>please enter character only</p>",
  contact_1_first_mess: "<p class='contact_2_firstcls rotate-90' style='color:red;'>please enter digit/character only</p>",
  contact_1_last_mess: "<p class='contact_2_lastcls rotate-90' style='color:red;'>please enter digit/character only</p>",
  contact_2_title_mess: "<p class='contact_2_titlecls rotate-90' style='color:red;'>please enter character only</p>",
  contact_2_first_mess: "<p class='contact_2_firstcls rotate-90' style='color:red;'>please enter digit/character only</p>",
  contact_2_last_mess: "<p class='contact_2_lastcls rotate-90' style='color:red;'>please enter digit/character only</p>",
  prop_man_title_mess: "<p class='prop_man_titlecls rotate-90' style='color:red;'>please enter character only</p>",
  prop_man_first_mess: "<p class='prop_man_firstcls rotate-90' style='color:red;'>please enter digit/character only</p>",
  prop_man_last_mess: "<p class='prop_man_lastcls rotate-90' style='color:red;'>please enter digit/character only</p>",
  phone_1_mess: "<p class='phone_1cls rotate-90' style='color:red;'>please enter the digit only</p>",
  phone_2_mess: "<p class='phone_2cls rotate-90' style='color:red;'>please enter the digit only</p>",
  mobile_mess: "<p class='mobilecls rotate-90' style='color:red;'>please enter the digit only</p>",
  mobile_2_mess: "<p class='mobile_2cls rotate-90' style='color:red;'>please enter the digit only</p>",
  fax_mess: "<p class='faxcls rotate-90' style='color:red;'>please enter the digit only</p>",

  c_title_mess: "<p class='faxcls rotate-90' style='color:red;'>please enter digit and character only.</p>",
  arrears_phone_mess: "<p class=' rotate-90' style='color:red;'>Please enter digit only </p>",
  invoice_contact_mess: "<p class=' rotate-90' style='color:red;'>Please enter digit only </p>",
  email_mess: "<p class='emailcls rotate-90' style='color:red;'>please enter correct format to validate email</p>",
  principal_email_mess: "<p class='principal_emailcls rotate-90' style='color:red;'>Please enter the right email format</p>",
  prop_man_email_mess: "<p class='prop_man_emailcls rotate-90' style='color:red;'>please enter correct format to validate email</p>",
  accounts_email_mess: "<p class=' rotate-90' style='color:red;'>please enter the valid email address </p>",
  pdf_email_mess: "<p class=' rotate-90' style='color:red;'>please enter the valid email address </p>",
  invoice_email_mess: "<p class='invoice_emailcls rotate-90' style='color:red;'>please enter correct format to validate email</p>",
  bdm_email_mess: "<p class=' rotate-90' style='color:red;'>please enter the valid email address </p>",
  abn_mess: "<p class='abncls rotate-90' style='color:red;'>please use digit only</p>",
  acn_mess: "<p class='acncls rotate-90' style='color:red;'>please use digit only</p>",
  user_name_mess: "<p class='user_namecls rotate-90' style='color:red;'>please enter digit only</p>",
  password_mess: "<p class='password_messcls rotate-90' style='color:red;'>Password (UpperCase, LowerCase and Number)</p>",
  num_props_mess: "<p class=' rotate-90' style='color:red;'>Please enter digit only </p>",
  car_rego_mess: "<p class=' rotate-90' style='color:red;'>Please enter digit only </p>",
  //props_as_at_mess:
  //renewal_date_mess:,

  cnote_year_mess: "<p class=' rotate-90' style='color:red;'>Please enter digit/character only </p>",
  status_mess: "<p class='statuscls rotate-90' style='color:red;'>on/off status bit</p>",
  //referral_val: "<p class='statuscls rotate-90' style='color:red;'>on/off status bit</p>",
  //platinum_partner_mess: "<p class=' rotate-90' style='color:red;'>on/off status bit</p>",
  // no_lapse_mess: "<p class=' rotate-90' style='color:red;'>on/off status bit</p>",
  invoice_type_mess: "<p class=' rotate-90' style='color:red;'>Please enter digit only </p>",


  // debtor 
  WORKADDRESS_mess: "<p class=' rotate-90' style='color:red;'>Please enter digit,char,+,-,#",
  service_address_mess: "<p class=' rotate-90' style='color:red;'>Please enter digit,char,+,-,# </p>",
  other_contact_mess: "<p class=' rotate-90' style='color:red;'>Please enter digit only </p>",
  drive_lic_num_mess: "<p class=' rotate-90' style='color:red;'>  please enter digit/character only</p>",
  passport_mess: "<p class=' rotate-90' style='color:red;'>Please enter digit/character only </p>",
  // interested_locating_mess:"<p class=' rotate-90' style='color:red;'>please on/off bit</p>",
  managing_agent_mess: "<p class=' rotate-90' style='color:red;'>Please enter digit/character only </p>",
  description_mess: "<p class=' rotate-90' style='color:red;'>Please enter digit/character only </p>",
  sex_mess: "<p class=' rotate-90' style='color:red;'>Please enter digit/character only </p>",

  DEFRELEST_mess: "<p class=' rotate-90' style='color:red;'>Please enter digit/character only </p>",

  AMNTOFDEF_mess: "<p class=' rotate-90' style='color:red;'>Please enter digit only</p>",

  paid_mess: "<p class=' rotate-90' style='color:red;'>please Enter digit only</p>",
  stillowing_mess: "<p class=' rotate-90' style='color:red;'>please Enter digit only</p>",

  //WORKADDRESS_mess:"<p class=' rotate-90' style='color:red;'>Please enter digit/character only </p>",
  WorkPosition_mess: "<p class=' rotate-90' style='color:red;'>Please enter digit/character only </p>",
  WorkHours_mess: "<p class=' rotate-90' style='color:red;'>Please enter digit/character only </p>",
};

  var validateDynamic =[
"bdm_email_3",
"invoice_email_3",
"pdf_email_3",
"accounts_email_3",
"email_3",
"arrears_phone_3",
"fax_3",
"mobile_3",
"phone_3"];

var validationArraydynamic = {
   bdm_email_3: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
   invoice_email_3: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
   pdf_email_3: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    accounts_email_3: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    email_3: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  arrears_phone_3: /^[0-9 ]{5,30}$/,
fax_3: /^[0-9 ]{5,15}$/,
mobile_3: /^[0-9 ]{5,15}$/,
phone_3: /^[0-9 ]{5,15}$/,
}

var validationmessagedynamic = {
 bdm_email_3: "<p class=' rotate-90' style='color:red;'>please enter the valid email address </p>",
  invoice_email_3: "<p class='invoice_emailcls rotate-90' style='color:red;'>please enter correct format to validate email</p>",
  pdf_email_3: "<p class=' rotate-90' style='color:red;'>please enter the valid email address </p>",
  accounts_email_3: "<p class=' rotate-90' style='color:red;'>please enter the valid email address </p>",
  email_3: "<p class='emailcls rotate-90' style='color:red;'>please enter correct format to validate email</p>",
  arrears_phone_3: "<p class=' rotate-90' style='color:red;'>Please enter digit only </p>",
fax_3: "<p class='faxcls rotate-90' style='color:red;'>please enter the digit only</p>",
mobile_3: "<p class='mobile_2cls rotate-90' style='color:red;'>please enter the digit only</p>",
phone_3: "<p class='phone_2cls rotate-90' style='color:red;'>please enter the digit only</p>",
};


$(document).ready(function () {
  $('input[class="checkboxonoff"]').change(function () {
    console.log($(this).attr("id"));
    this.value = (Number(this.checked));
    console.log((Number(this.checked)));
  });
  console.log("user_id : " + id);
  representdataclientApi(id);

  var readURL = function (input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('.profile-pic').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }
  $(".file-upload").on('change', function () {
    readURL(this);
  });

  $(".upload-button").on('click', function () {
    $(".file-upload").click();
  });


  $(".form-CDO2-left").click(function () {
    $('.rotate-90').remove();
    var flag = 0;
    //var yourArray = ["client_id","name","contact_1_first","contact_1_last","phone_1","mobile","email","WORKADDRESS","service_address","other_contact","drive_lic_num","passport","DEFRELEST","AMNTOFDEF","WorkPosition","WorkHours"];

    // for (i = 0; i < yourArray.length; i++) {
    //   var testval = yourArray[i] + '_val';
    //   var testmes = yourArray[i] + '_mess';
    //   var arrayindx = $('#' + yourArray[i]).val();

    //   if (validationArray[testval] != undefined) {
    //     if (!validationArray[testval].test(arrayindx)) {
    //       $('#' + yourArray[i]).after(validationmessage[testmes]);
    //       flag = 1;
    //     }
    //   }
    // }
    if (flag == 1) {
      return false;
    } else {
      leftArrow();
    }

  });

  $(".form-CDO2-right").click(function () {
    $('.rotate-90').remove();
    var flag = 0;
    //var yourArray = ["client_id","name","contact_1_first","contact_1_last","phone_1","mobile","email","WORKADDRESS","service_address","other_contact","drive_lic_num","passport","DEFRELEST","AMNTOFDEF","WorkPosition","WorkHours"];

    // for (i = 0; i < yourArray.length; i++) {
    //   var testval = yourArray[i] + '_val';
    //   var testmes = yourArray[i] + '_mess';
    //   var arrayindx = $('#' + yourArray[i]).val();

    //   if (validationArray[testval] != undefined) {
    //     if (!validationArray[testval].test(arrayindx)) {
    //       $('#' + yourArray[i]).after(validationmessage[testmes]);
    //       flag = 1;
    //     }
    //   }
    // }
    if (flag == 1) {
      return false;
    } else {
      rightArrow();
    }

  });

  $(".form-CDO1-left").click(function () {
    // leftArrow();
    $('.rotate-90').remove();
    var flag = 0;
    //var yourArray = ["client_id","name","contact_1_first","contact_1_last","phone_1","mobile","email","WORKADDRESS","service_address","other_contact","drive_lic_num","passport","DEFRELEST","AMNTOFDEF","WorkPosition","WorkHours"];
 
    // for (i = 0; i < yourArray.length; i++) {
    //   var testval = yourArray[i] + '_val';
    //   var testmes = yourArray[i] + '_mess';
    //   var arrayindx = $('#' + yourArray[i]).val();

    //   if (validationArray[testval] != undefined) {
    //     if (!validationArray[testval].test(arrayindx)) {
    //       $('#' + yourArray[i]).after(validationmessage[testmes]);
    //       flag = 1;
    //     }
    //   }
    // }
    if (flag == 1) {
      return false;
    } else {
      leftArrow();
    }
  });

  $(".form-CDO1-right").click(function () {
    // rightArrow();
    $('.rotate-90').remove();
    var flag = 0;
    //var yourArray = ["client_id","name","contact_1_first","contact_1_last","phone_1","mobile","email","WORKADDRESS","service_address","other_contact","drive_lic_num","passport","DEFRELEST","AMNTOFDEF","WorkPosition","WorkHours"];

   
    // for (i = 0; i < yourArray.length; i++) {
    //   var testval = yourArray[i] + '_val';
    //   var testmes = yourArray[i] + '_mess';
    //   var arrayindx = $('#' + yourArray[i]).val();

    //   if (validationArray[testval] != undefined) {
    //     if (!validationArray[testval].test(arrayindx)) {
    //       $('#' + yourArray[i]).after(validationmessage[testmes]);
    //       flag = 1;
    //     }
    //   }
    // }
    if (flag == 1) {
      return false;
    } else {
      rightArrow();
    }
  });

  /*left arrow click*/
  function leftArrow() {
    var newid;
    var lenstring = apiurl.length;
    var start = apiurl.lastIndexOf("/");
    var res = apiurl.substring(parseInt(start) + 1, lenstring);
    var inde = $.inArray(parseInt(res), userdata);
    if ($.inArray(parseInt(res), userdata) > 0) {
      if (inde > 0) {
        newid = userdata[parseInt(inde) - 1];
      } else {
        newid = userdata[parseInt(inde)];
      }
      id = newid;
      representdataclientApi(id);

    } else {
      alert("search record end");
    }
  }
  /*right arrow click*/
  function rightArrow() {
    var newid;
    var lenstring = apiurl.length;
    var start = apiurl.lastIndexOf("/");
    var res = apiurl.substring(parseInt(start) + 1, lenstring);
    var inde = $.inArray(parseInt(res), userdata);
    // console.log(userdata.length);
    console.log("index " + inde);
    if ((userdata.length - 2) >= inde) {
      newid = userdata[parseInt(inde) + 1];
      id = newid;
      representdataclientApi(id);
    } else {
      alert("search record end");
    }
  }

  /*cleint/Agent  form submit*/
  $('#form-CDO1').submit(function (e) {
    $('.rotate-90').remove();
    var flag = 0;
    e.preventDefault();

    var yourArray = ["client_code", "name","title_note","sms_name", "address_1", "address_2","street","suburb",
    "abn", "acn", "user_name", "num_props", "invoice_type",  
    "prop_man_title", "prop_man_first", "prop_man_last","prop_man_email", 
    "contact_2_title","contact_2_first","contact_2_last",
    "post_code", "contact_1_title","contact_1_first", "contact_1_last", "principal_email","phone_1", "mobile","c_title"
    ];

    var yourArraydata = [];
    yourArray.forEach(function (i, a) {
      if ($('#' + i).val() != '') {
        //console.log(i);
        yourArraydata.push(i);
      }
    });
    // $("input:checkbox[name=type]:checked").each(function() {
    // var getid=$(this).attr("id");
    // //yourArray.push(getid.replace("_c",""));
    // yourArray.push(getid.substring("0",getid.length-2));
    // //console.log($(this).attr("id"));
    // });
    //console.log(yourArray);
    for (i = 0; i < yourArraydata.length; i++) {
      var testval = yourArraydata[i] + '_val';
      var testmes = yourArraydata[i] + '_mess';
      var arrayindx = $('#' + yourArraydata[i]).val();

      if (validationArray[testval] != undefined) {
        if (!validationArray[testval].test(arrayindx)) {
          $('#' + yourArraydata[i]).after(validationmessage[testmes]);
          flag = 1;
        }
      }
    }

var coout;
    for(k=0; k< validateDynamic.length; k++){
     coout =k;
     $('.'+validateDynamic[coout] ).each(function(index,value ) {
    
      var valdata = $(this).val();
     
      if($(this).val() && $(this).val() !='' ){
          var ar_data= validateDynamic[coout];
       if (!validationArraydynamic[ar_data].test($(this).val())) {
          $(this).after(validationmessagedynamic[ar_data]);
          flag = 1;
        }
      }
       coout++;
    });
   }


    if (flag == 1) {
      return false;
    } else {
      var checkboxdatacleint = "&referral=" + $('#referral').val() + "&platinum_partner=" + $('#platinum_partner').val() + "&no_lapse=" + $('#no_lapse').val();
      //alert("form validate : these valuse are saved to the postgres database");
      console.log($('#form-CDO1').serialize() + checkboxdatacleint);
      $.ajax({
        type: 'POST',
        url: 'http://159.65.139.47:8899/client',
        data: $('#form-CDO1').serialize() + checkboxdatacleint,
        success: function (data) {
          console.log("success 1");
          //alert(JSON.stringify(data));
          alert(data.message);
        },
        error: function (data) {
          console.log("error");
          alert(JSON.stringify(data));
        }
      });
    }
  });

  $('#form-CDO2').submit(function (e) {
    $('.rotate-90').remove();
    var flag = 0;
    e.preventDefault();
    var yourArray = ['client_id', 'name', 'contact_1_first', 'contact_1_last', 'email', 'sex', 'phone_1', 'mobile', 'other_contact', 'work', 'WORKADDRESS', 'managing_agent', 'description', 'AMNTOFDEF', 'dob', 'date_of_entry', 'service_address', 'drive_lic_num', 'passport', 'car_rego', 'EvictionStartDate'];
    // var yourArray = ["client_id","name","contact_1_first","contact_1_last","phone_1","mobile","email","WORKADDRESS","service_address","other_contact","drive_lic_num","passport","DEFRELEST","AMNTOFDEF","WorkPosition","WorkHours"];

    // $("input:checkbox[name=type]:checked").each(function() {
    // var getid=$(this).attr("id");
    // //yourArray.push(getid.replace("_c",""));
    // yourArray.push(getid.substring("0",getid.length-2));
    // //console.log($(this).attr("id"));
    // });
    //console.log(yourArray);
    var yourArraydata = [];
    yourArray.forEach(function (i, a) {
      if ($('#' + i).val() != '') {
        //console.log(i);
        yourArraydata.push(i);
      }
    });
    // console.log(yourArraydata);
    for (i = 0; i < yourArraydata.length; i++) {
      var testval = yourArraydata[i] + '_val';
      var testmes = yourArraydata[i] + '_mess';
      var arrayindx = $('#' + yourArraydata[i]).val();

      if (validationArray[testval] != undefined) {
        if (!validationArray[testval].test(arrayindx)) {
          $('#' + yourArraydata[i]).after(validationmessage[testmes]);
          flag = 1;
        }
      }
    }
    if (flag == 1) {
      return false;
    } else {
      // alert("form validate : these valuse are saved to the postgres database");
      var checkboxdata = "&eviction=" + $('#eviction').val() + "&WithholdData=" + $('#WithholdData').val();
      console.log($('#form-CDO2').serialize() + checkboxdata);
      $.ajax({
        type: 'POST',
        url: 'http://159.65.139.47:8899/debator',
        // headers: {"Allow-Control-Allow-Origin" : '*'},
        data: $('#form-CDO2').serialize() + checkboxdata,
        success: function (data) {
          console.log("success");
          alert(data.message);
        },
        error: function (data) {
          console.log("error");
          alert(JSON.stringify(data));
        }
      });
      //

    }
  });

});