 $(document).ready(function () {
 var yourwip=["wip_id",
"file_number",
"client_id",
"client_code",
"lodged",
"category",
"finalize",
"origin_state",
"file_location",
"urgency",
"client_ref",
"send_owner_reports",
"cover_as_at_date_log",
"cover",
"paid_out",
"paid_out_date",
"purchased_amount",
"residual_basic_claim",
"offered_amount",
"calculate_interest",
"total_received",
"incured_from",
"incured_to",
"judgement_date",
"served_date",
"debt_cleared_by_tenant",
"debt_cleared_by_bankruptcy",
"debt_cleared_by_insurance",
"debt_cleared_by_other",
"payment_arranged",
"amount_received",
"part_payment_period",
"payment_by",
"last_payment",
"special_payment",
"property_id",
"plaintiff_name",
"plaintiff_address",
"defendant_name",
"defendant_address",
"order_location",
"order_date",
"order_claim_number",
"plaintiff_number",
"court_location",
"enforcement_date",
"claim_amount",
"recoverable_costs",
"non_recoverable_costs",
"notes",
"audit_notes",
"multi_judgement",
"interest_owing",
"old_prop_id",
"agent_email",
"agent_name",
"priority",
"form17_sent",
"pay_owner_direct",
"owner_name",
"owner_address",
"owner_contact",
"owner_email",
"owner_bank",
"insure_had",
"insure_by",
"insure_amount",
"insure_total_claim",
"insure_nill_payout",
"requested_taf",
"requested_order"];



var validatearray=[
"wip_id",
"file_number",
"client_id",
"origin_state",
"urgency",
"send_owner_reports",
"property_id",
"multi_judgement",
"old_prop_id",
"priority",
"requested_order",
"insure_had",
"paid_out",
"purchased_amount",
"residual_basic_claim",
"offered_amount",
"calculate_interest",
"total_received",
"debt_cleared_by_tenant",
"debt_cleared_by_bankruptcy",
"debt_cleared_by_insurance",
"debt_cleared_by_other",
"payment_arranged",
"amount_received",
"part_payment_period",
"special_payment",
"claim_amount",
"recoverable_costs",
"non_recoverable_costs",
"interest_owing",
"pay_owner_direct",
"insure_amount",
"insure_total_claim",
"insure_nill_payout",
"requested_taf"
];
var validae_wip = {
  wip_id: /^[0-9]{1,15}$/,
file_number: /^[0-9]{1,15}$/,
client_id: /^[0-9]{1,15}$/,
origin_state: /^[0-9]{1,15}$/,
urgency: /^[0-9]{1,15}$/,
send_owner_reports: /^[0-9]{1,15}$/,
property_id: /^[0-9]{1,15}$/,
multi_judgement: /^[0-9]{1,15}$/,
old_prop_id: /^[0-9]{1,15}$/,
priority: /^[0-9]{1,15}$/,
requested_order: /^[0-9]{1,15}$/,
insure_had: /^[0-9]{1,15}$/,

paid_out :/^[-+]?[0-9]*\.?[0-9]+$/,
purchased_amount:/^[-+]?[0-9]*\.?[0-9]+$/,
residual_basic_claim:/^[-+]?[0-9]*\.?[0-9]+$/,
offered_amount:/^[-+]?[0-9]*\.?[0-9]+$/,
calculate_interest:/^[-+]?[0-9]*\.?[0-9]+$/,
total_received:/^[-+]?[0-9]*\.?[0-9]+$/,
debt_cleared_by_tenant:/^[-+]?[0-9]*\.?[0-9]+$/,
debt_cleared_by_bankruptcy:/^[-+]?[0-9]*\.?[0-9]+$/,
debt_cleared_by_insurance:/^[-+]?[0-9]*\.?[0-9]+$/,
debt_cleared_by_other:/^[-+]?[0-9]*\.?[0-9]+$/,
payment_arranged:/^[-+]?[0-9]*\.?[0-9]+$/,
amount_received:/^[-+]?[0-9]*\.?[0-9]+$/,
part_payment_period:/^[-+]?[0-9]*\.?[0-9]+$/,
special_payment:/^[-+]?[0-9]*\.?[0-9]+$/,
claim_amount:/^[-+]?[0-9]*\.?[0-9]+$/,
recoverable_costs:/^[-+]?[0-9]*\.?[0-9]+$/,
non_recoverable_costs:/^[-+]?[0-9]*\.?[0-9]+$/,
interest_owing:/^[-+]?[0-9]*\.?[0-9]+$/,
pay_owner_direct:/^[-+]?[0-9]*\.?[0-9]+$/,
insure_amount:/^[-+]?[0-9]*\.?[0-9]+$/,
insure_total_claim:/^[-+]?[0-9]*\.?[0-9]+$/,
insure_nill_payout:/^[-+]?[0-9]*\.?[0-9]+$/,
requested_taf:/^[-+]?[0-9]*\.?[0-9]+$/,
};

var validationmessage={
 wip_id : "<p class='client_codecls rotate-90' style='color:red;'>please enter digit only</p>",
file_number : "<p class='client_codecls rotate-90' style='color:red;'>please enter digit only</p>",
client_id : "<p class='client_codecls rotate-90' style='color:red;'>please enter digit only</p>",
origin_state : "<p class='client_codecls rotate-90' style='color:red;'>please enter digit only</p>",
urgency : "<p class='client_codecls rotate-90' style='color:red;'>please enter digit only</p>",
send_owner_reports : "<p class='client_codecls rotate-90' style='color:red;'>please enter digit only</p>",
property_id : "<p class='client_codecls rotate-90' style='color:red;'>please enter digit only</p>",
multi_judgement : "<p class='client_codecls rotate-90' style='color:red;'>please enter digit only</p>",
old_prop_id : "<p class='client_codecls rotate-90' style='color:red;'>please enter digit only</p>",
priority : "<p class='client_codecls rotate-90' style='color:red;'>please enter digit only</p>",
requested_order : "<p class='client_codecls rotate-90' style='color:red;'>please enter digit only</p>",
insure_had : "<p class='client_codecls rotate-90' style='color:red;'>please enter digit only</p>",

paid_out:"<p class='client_codecls rotate-90' style='color:red;'>please enter correct amount</p>",
purchased_amount:"<p class='client_codecls rotate-90' style='color:red;'>please enter correct amount</p>",
residual_basic_claim:"<p class='client_codecls rotate-90' style='color:red;'>please enter correct amount</p>",
offered_amount:"<p class='client_codecls rotate-90' style='color:red;'>please enter correct amount</p>",
calculate_interest:"<p class='client_codecls rotate-90' style='color:red;'>please enter correct amount</p>",
total_received:"<p class='client_codecls rotate-90' style='color:red;'>please enter correct amount</p>",
debt_cleared_by_tenant:"<p class='client_codecls rotate-90' style='color:red;'>please enter correct amount</p>",
debt_cleared_by_bankruptcy:"<p class='client_codecls rotate-90' style='color:red;'>please enter correct amount</p>",
debt_cleared_by_insurance:"<p class='client_codecls rotate-90' style='color:red;'>please enter correct amount</p>",
debt_cleared_by_other:"<p class='client_codecls rotate-90' style='color:red;'>please enter correct amount</p>",
payment_arranged:"<p class='client_codecls rotate-90' style='color:red;'>please enter correct amount</p>",
amount_received:"<p class='client_codecls rotate-90' style='color:red;'>please enter correct amount</p>",
part_payment_period:"<p class='client_codecls rotate-90' style='color:red;'>please enter correct amount</p>",
special_payment:"<p class='client_codecls rotate-90' style='color:red;'>please enter correct amount</p>",
claim_amount:"<p class='client_codecls rotate-90' style='color:red;'>please enter correct amount</p>",
recoverable_costs:"<p class='client_codecls rotate-90' style='color:red;'>please enter correct amount</p>",
non_recoverable_costs:"<p class='client_codecls rotate-90' style='color:red;'>please enter correct amount</p>",
interest_owing:"<p class='client_codecls rotate-90' style='color:red;'>please enter correct amount</p>",
pay_owner_direct:"<p class='client_codecls rotate-90' style='color:red;'>please enter correct amount</p>",
insure_amount:"<p class='client_codecls rotate-90' style='color:red;'>please enter correct amount</p>",
insure_total_claim:"<p class='client_codecls rotate-90' style='color:red;'>please enter correct amount</p>",
insure_nill_payout:"<p class='client_codecls rotate-90' style='color:red;'>please enter correct amount</p>",
requested_taf:"<p class='client_codecls rotate-90' style='color:red;'>please enter correct amount</p>",

}
represent_wipapi();
function represent_wipapi(){
  var wipurl="https://intranet.barclaymis.com.au/v1/wip/100";
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
  if (this.readyState == 4 && this.status == 200){   
     var res = JSON.parse(this.responseText);
            for (i = 0; i < yourwip.length; i++) { 
              // console.log(formdataClient[i]);
                if(res[yourwip[i]]!= null){
              if(res[yourwip[i]] != "" && res[yourwip[i]] != undefined && res[yourwip[i]] != "0000-00-00" ){//yourArray.push(formdataClient[i]);
                 document.getElementById(yourwip[i]).value=res[yourwip[i]];
                 //console.log(res[yourwip[i]]);
               }
            
              }
            }
        }
};
xhttp.open("GET",wipurl, true);
xhttp.send();
}


$('#form-wip').submit(function (e) {
    $('.rotate-90').remove();
    var flag = 0;
    e.preventDefault();

     for (i = 0; i < validatearray.length; i++) {
      var testval = validatearray[i];
      var testmes = validatearray[i];
      var arrayindx = $('#'+validatearray[i]).val();

      if (validae_wip[testval] != undefined) {
        if (!validae_wip[testval].test(arrayindx)) {
          $('#' + validatearray[i]).after(validationmessage[testmes]);
          flag = 1;
        }
      }
    } 
    if (flag == 1) {
      return false;
    }else {
       console.log($('#form-wip').serialize()); 
       alert("ready to submit data!");
      }
  
  });

});