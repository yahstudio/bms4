 $(document).ready(function () {
 var your_debtorwip=["wip_debtor_id",
"wip_id",
"file_number",
"debtor_id",
"judgement_date",
"served_date",
"payment_arranged",
"part_payment_period",
"payment_by",
"amount_recived",
"total_received",
"last_payment",
"special_payment",
"current_claim",
"recoverable_costs",
"total_outstanding",
"costs",
"fees",
"debt_cleared_by_tenant",
"debt_cleared_by_insurance",
"debt_cleared_by_bankruptcy",
"debt_cleared_by_other",
"special_payment_notes",
"fraction",
"court_location",
"plaintiff_number",
"enforcement_date"
];

var validatearray =[
"wip_debtor_id",
"wip_id",
"file_number",
"debtor_id",
"fraction",
"payment_arranged",
"part_payment_period",
"payment_by",
"amount_recived",
"total_received",
"special_payment",
"current_claim",
"recoverable_costs",
"total_outstanding",
"costs",
"fees",
"debt_cleared_by_tenant",
"debt_cleared_by_insurance",
"debt_cleared_by_bankruptcy",
"debt_cleared_by_other"
];
var validae_debtrwip = {
wip_debtor_id_val: /^[0-9]{1,15}$/,
wip_id_val: /^[0-9]{1,15}$/,
file_number_val: /^[0-9]{1,15}$/,
debtor_id_val: /^[0-9]{1,15}$/,
fraction_val: /^[0-9]{1,15}$/,
payment_arranged_val: /^[0-9]{1,15}$/,
part_payment_period_val:/^[a-zA-Z ]{3,30}$/,
payment_by_val:/^[a-zA-Z ]{3,30}$/,

amount_recived_val:/^[-+]?[0-9]*\.?[0-9]+$/,
total_received_val:/^[-+]?[0-9]*\.?[0-9]+$/,
special_payment_val:/^[-+]?[0-9]*\.?[0-9]+$/,
current_claim_val:/^[-+]?[0-9]*\.?[0-9]+$/,
recoverable_costs_val:/^[-+]?[0-9]*\.?[0-9]+$/,
total_outstanding_val:/^[-+]?[0-9]*\.?[0-9]+$/,
costs_val:/^[-+]?[0-9]*\.?[0-9]+$/,
fees_val:/^[-+]?[0-9]*\.?[0-9]+$/,
debt_cleared_by_tenant_val:/^[-+]?[0-9]*\.?[0-9]+$/,
debt_cleared_by_insurance_val:/^[-+]?[0-9]*\.?[0-9]+$/,
debt_cleared_by_bankruptcy_val:/^[-+]?[0-9]*\.?[0-9]+$/,
debt_cleared_by_other_val:/^[-+]?[0-9]*\.?[0-9]+$/,
};

var validationmessage={
  wip_debtor_id_mess: "<p class='client_codecls rotate-90' style='color:red;'>please enter digit only</p>",
  wip_id_mess : "<p class='client_codecls rotate-90' style='color:red;'>please enter digit only</p>",
  file_number_mess:"<p class='client_codecls rotate-90' style='color:red;'>please enter digit only</p>",
debtor_id_mess: "<p class='client_codecls rotate-90' style='color:red;'>please enter digit only</p>",
fraction_mess: "<p class='client_codecls rotate-90' style='color:red;'>please enter digit only</p>",
payment_arranged_mess: "<p class='client_codecls rotate-90' style='color:red;'>please enter digit only</p>",
part_payment_period_mess:"<p class='rotate-90' style='color:red;'>please enter character only</p>",
payment_by_mess:"<p class='rotate-90' style='color:red;'>please enter character only</p>",

amount_recived_mess:"<p class='client_codecls rotate-90' style='color:red;'>please enter correct amount</p>",
total_received_mess:"<p class='client_codecls rotate-90' style='color:red;'>please enter correct amount</p>",
special_payment_mess:"<p class='client_codecls rotate-90' style='color:red;'>please enter correct amount</p>",
current_claim_mess:"<p class='client_codecls rotate-90' style='color:red;'>please enter correct amount</p>",
recoverable_costs_mess:"<p class='client_codecls rotate-90' style='color:red;'>please enter correct amount</p>",
total_outstanding_mess:"<p class='client_codecls rotate-90' style='color:red;'>please enter correct amount</p>",
costs_mess:"<p class='client_codecls rotate-90' style='color:red;'>please enter correct amount</p>",
fees_mess:"<p class='client_codecls rotate-90' style='color:red;'>please enter correct amount</p>",
debt_cleared_by_tenant_mess:"<p class='client_codecls rotate-90' style='color:red;'>please enter correct amount</p>",
debt_cleared_by_insurance_mess:"<p class='client_codecls rotate-90' style='color:red;'>please enter correct amount</p>",
debt_cleared_by_bankruptcy_mess:"<p class='client_codecls rotate-90' style='color:red;'>please enter correct amount</p>",
debt_cleared_by_other_mess:"<p class='client_codecls rotate-90' style='color:red;'>please enter correct amount</p>",
}
represent_debter_wipapi();
function represent_debter_wipapi(){
  var wipdebtorurl="https://intranet.barclaymis.com.au/v1/debtor_wip/15447";
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
  if (this.readyState == 4 && this.status == 200){   
     var res = JSON.parse(this.responseText);
            for (i = 0; i < your_debtorwip.length; i++) { 
               
                if(res[0][your_debtorwip[i]]!= null){
                  //console.log(res[0][your_debtorwip[i]]);
              if(res[0][your_debtorwip[i]] != "" && res[0][your_debtorwip[i]] != undefined && res[0][your_debtorwip[i]] != "0000-00-00"  ){//yourArray.push(formdataClient[i]);
                 document.getElementById(your_debtorwip[i]).value=res[0][your_debtorwip[i]];
                 
               }
            
              }
            }
        }
};
xhttp.open("GET",wipdebtorurl, true);
xhttp.send();
}


$('#form-dbtrwip').submit(function (e) {
    $('.rotate-90').remove();
    var flag = 0;
    e.preventDefault();

     for (i = 0; i < validatearray.length; i++) {
      var testval = validatearray[i]+ '_val';
      var testmes = validatearray[i]+ '_mess';
      var arrayindx = $('#'+validatearray[i]).val();

      if (validae_debtrwip[testval] != undefined) {
        if (!validae_debtrwip[testval].test(arrayindx)) {
          $('#' + validatearray[i]).after(validationmessage[testmes]);
          flag = 1;
        }
      }
    } 
    if (flag == 1) {
      return false;
    }else {
       console.log($('#form-dbtrwip').serialize());
        alert("ready to submit data!");
          
      }
  
  });

});