#!/usr/bin/python3
from flask import Response
from flask import Flask, render_template, request, url_for, redirect
import datetime
import json, requests

app = Flask(__name__)

import psycopg2
from psycopg2.extras import DictCursor
from psycopg2.extras import RealDictCursor

psycopg2.extras.register_default_json(loads=lambda x: x)

# This block is used for making connection with database.
def db_open():
	global cur
	global conn
	conn = psycopg2.connect(database="dummy_db", user = "postgres", password = "EWZhNH2Vqz@P74!Q", host = "127.0.0.1", port = "5432")
	cur = conn.cursor(cursor_factory=RealDictCursor)

# This block is used to JSON output according to passed data.
def out_json(data):
	resp = Response(data)
	resp.headers['Content-type'] = 'application/json'
	resp.headers['Access-Control-Allow-Origin'] = '*'
	return resp

# This block is used to Add user into database based on some fields.
@app.route('/add/', methods=['POST','GET'])
def add_user():
	if request.method == 'POST':
		try:
			name = request.form['name']
			age = request.form['age']
			salary = request.form['salary']
			post = request.form['post']
			db_open()
			cur.execute('insert into person(Name,Age,Salary,Post) values(%s, %s, %s, %s);', (name, age, salary, post,))
			conn.commit()

		except Exception as e:
			pass

		return render_template('add.html')
	if request.method == 'GET':
		return render_template('add.html')

# This block is will return all data of person table.
@app.route("/all_data/")
def all_data():
	db_open()
	data = {"person" : ""}
	cur.execute('SELECT * FROM person ;')
	data["person"] = cur.fetchall()

	dataset = json.dumps(data, default=str);

	return out_json(dataset)

# This block is used to displaing the login screen.
@app.route('/')
def login():
	return render_template('barclay/login.html')

# This block is used to displaing the dashboard screen. At initial stage we are taking 5 clients[having ID 1,8,9,15,16] details to list on the dashboard.
@app.route('/dashboard/')
def dashboard():
	id_list = [1,8,9,15,16]
	client_data=[]
	for i in id_list:
		data={}
		page = requests.get('https://intranet.barclaymis.com.au/v1/client/'+str(i))
		page_data = json.loads(page.text)
		data['client_id'] = page_data['client_id']
		data['name'] = page_data['name']
		data['renewal_date'] = page_data['renewal_date']
		client_data.append(data)

	return render_template('barclay/dashboard.html',client_data=client_data)

# This block will return the client information based on there client id. If data is exist into DB for the given client ID than data will fetched form DB
# Otherwise data will come from BMS3 client API
# If user update client information and save this than we first delete the existing data than save fresh data into DB.
@app.route('/client_info/<int:client_id>', methods=['POST','GET'])
def client_info(client_id):

	if request.method == 'POST':
		db_open()

		client_id =int(client_id)
		client_code = request.form['client_code']
		title_note = request.form['title_note']
		sms_name = request.form['sms_name']
		phone_1 = request.form['phone_1']
		phone_2 = request.form['phone_2']
		acn = request.form['acn']
		abn = request.form['abn']


		try:
			cur.execute('delete from contact_client_overview_info where client_id = '+str(client_id)+';')
			conn.commit()
			cur.execute('delete from contact_client_overview_address where client_id = '+str(client_id)+';')
			conn.commit()
			cur.execute('delete from contact_contact_detail where client_id = '+str(client_id)+';')
			conn.commit()
			cur.execute('delete from contact_account_overview where client_id = '+str(client_id)+';')
			conn.commit()
		except Exception as e:
			pass
		
		cur.execute('insert into contact_client_overview_info(client_id,client_code,title_note,sms_name,phone_1,phone_2,acn,abn) values(%s, %s, %s, %s, %s, %s, %s, %s);', (client_id,client_code,title_note,sms_name,phone_1,phone_2,acn,abn,))
		conn.commit()

		address_1 = request.form.getlist('address_1[]')
		address_2 = request.form.getlist('address_2[]')
		state = request.form.getlist('state[]')
		post_code = request.form.getlist('post_code[]')
		country = request.form.getlist('country[]')

		for i in range(len(address_1)):
			cur.execute('insert into contact_client_overview_address(client_id,client_code,address_1,address_2,state,post_code,country) values(%s, %s, %s, %s, %s, %s, %s);', (client_id,client_code,address_1[i],address_2[i],state[i],post_code[i],country[i],))
			conn.commit()


		contact_1_title = request.form.getlist('contact_1_title[]')
		contact_1_first = request.form.getlist('contact_1_first[]')
		contact_1_last = request.form.getlist('contact_1_last[]')
		contact_email = request.form.getlist('contact_email[]')
		contact_mobile = request.form.getlist('contact_mobile[]')
		contact_fax = request.form.getlist('contact_fax[]')

		for i in range(len(address_1)):
			cur.execute('insert into contact_contact_detail(client_id,client_code,contact_1_title,contact_1_first,contact_1_last,email,mobile,fax) values(%s, %s, %s, %s, %s, %s, %s, %s);', (client_id,client_code,contact_1_title[i],contact_1_first[i],contact_1_last[i],contact_email[i],contact_mobile[i],contact_fax[i],))
			conn.commit()

		client_code = request.form['client_code']
		num_props = request.form['num_props']
		invoice_type = request.form['invoice_type']
		cnote_year = request.form['cnote_year']

		cur.execute('insert into contact_account_overview(client_id,client_code,num_props,invoice_type,cnote_year) values(%s, %s, %s, %s, %s);', (client_id,client_code,num_props,invoice_type,cnote_year,))
		conn.commit()

		return redirect(url_for('client_info',client_id=client_id))

	if request.method == 'GET':
		
		db_open()
		data = {"client_overview_info" : "", "client_overview_address" : "", "contact_detail" : "", "account_overview" : ""}

		cur.execute('SELECT * FROM contact_client_overview_info where client_id='+str(client_id)+';')
		data["client_overview_info"] = cur.fetchall()

		cur.execute('SELECT * FROM contact_client_overview_address where client_id='+str(client_id)+';')
		data["client_overview_address"] = cur.fetchall()

		cur.execute('SELECT * FROM contact_contact_detail where client_id='+str(client_id)+';')
		data["contact_detail"] = cur.fetchall()

		cur.execute('SELECT * FROM contact_account_overview where client_id='+str(client_id)+';')
		data["account_overview"] = cur.fetchall()

		dataset = json.dumps(data);
		try:		
			client_data={}

			client_data['client_code'] = data['client_overview_info'][0]['client_code']
			client_data['title_note'] = data['client_overview_info'][0]['title_note']
			client_data['sms_name'] = data['client_overview_info'][0]['sms_name']
			client_data['phone_1'] = data['client_overview_info'][0]['phone_1']
			client_data['phone_2'] = data['client_overview_info'][0]['phone_2']
			client_data['acn'] = data['client_overview_info'][0]['acn']
			client_data['abn'] = data['client_overview_info'][0]['abn']

			client_data['address_1'] = data['client_overview_address'][0]['address_1']
			client_data['address_2'] = data['client_overview_address'][0]['address_2']
			client_data['state'] = data['client_overview_address'][0]['state']
			client_data['post_code'] = data['client_overview_address'][0]['post_code']
			client_data['country'] = data['client_overview_address'][0]['country']

			client_data['contact_1_title'] = data['contact_detail'][0]['contact_1_title']
			client_data['contact_1_first'] = data['contact_detail'][0]['contact_1_first']
			client_data['contact_1_last'] = data['contact_detail'][0]['contact_1_last']
			client_data['email'] = data['contact_detail'][0]['email']
			client_data['mobile'] = data['contact_detail'][0]['mobile']
			client_data['fax'] = data['contact_detail'][0]['fax']


			client_data['num_props'] = data['account_overview'][0]['num_props']
			client_data['client_data'] = data['account_overview'][0]['invoice_type']
			client_data['cnote_year'] = data['account_overview'][0]['cnote_year']
		except Exception as e:
			page = requests.get('https://intranet.barclaymis.com.au/v1/client/'+str(client_id))
			client_data = json.loads(page.text)

		return render_template('barclay/client_info.html',client_data=client_data)

# This block will render task.html
@app.route('/task/')
def task():
	return render_template('barclay/task.html')

# This block will render claim.html
@app.route('/claim/')
def claim():
	return render_template('old_html/claim.html')

# This block will render client.html
@app.route('/client/')
def client():
	return render_template('old_html/client.html')

# This block will render debtors.html
@app.route('/debtors/')
def debtors():
	return render_template('old_html/debtors.html')

# This block will render costs.html
@app.route('/costs/')
def costs():
	return render_template('old_html/costs.html')

# This block will render debter_wip.html
@app.route('/debter_wip/')
def debter_wip():
	return render_template('old_html/debter_wip.html')

# This block will render dispersal_note.html
@app.route('/dispersal_note/')
def dispersal_note():
	return render_template('old_html/dispersal_note.html')

# This block will render important_wip_note.html
@app.route('/important_wip_note/')
def important_wip_note():
	return render_template('old_html/important_wip_note.html')

# This block will render paid_to_agent.html
@app.route('/paid_to_agent/')
def paid_to_agent():
	return render_template('old_html/paid_to_agent.html')

# This block will render pay_plan.html
@app.route('/pay_plan/')
def pay_plan():
	return render_template('old_html/pay_plan.html')

# This block will render transaction.html
@app.route('/transaction/')
def transaction():
	return render_template('old_html/transaction.html')

# This block will render wip.html
@app.route('/wip/')
def wip():
	return render_template('old_html/wip.html')

# This block will render wnote.html
@app.route('/wnote/')
def wnote():
	return render_template('old_html/wnote.html')

# This block will render wstaff.html
@app.route('/wstaff/')
def wstaff():
	return render_template('old_html/wstaff.html')

app.config['DEBUG'] = False

if __name__ == "__main__":
	app.secret_key = 'asdf8sdufsd8f73jhnr7dyfsdfish%^$dfds'
	app.run()
