from flask_sqlalchemy import SQLAlchemy
from flask_sqlalchemy import sqlalchemy

import datetime

db = SQLAlchemy()

class ClientInfo(db.Model):
	__tablename__ = 'ClientInfo'
	id = db.Column(db.Integer, primary_key=True)
	client_code = db.Column(db.String(20), unique=True)
	name = db.Column(db.String(100))
	group = db.Column(db.String(100))
	abn = db.Column(db.String(100))
	acn = db.Column(db.String(100))
	title = db.Column(db.String(100))
	invoice_email = db.Column(db.String(100))
	invoice_type = db.Column(db.String(100))
	username = db.Column(db.String(100))
	password = db.Column(db.String(100))
	sms_name = db.Column(db.String(15))
	contact_2_title = db.Column(db.String(100))
	contact_2_first = db.Column(db.String(100))
	contact_2_last = db.Column(db.String(100))
	status = db.Column(db.String(15))
	addresses = db.relationship('ClientAddress', backref='ClientInfo', lazy=True)
	client_prop_info = db.relationship('ClientPropInfo', backref='ClientInfo', lazy=True)
	principle_info = db.relationship('PrincipleInfo', backref='ClientInfo', lazy=True)
	subscription_info = db.relationship('Subscription', backref='ClientInfo', lazy=True)

	created_at = db.Column(db.DateTime, nullable=True)

	def __init__(self,client_code,name,group,abn,acn,invoice_email,invoice_type,username,password,title,sms_name,contact_2_title,contact_2_first,contact_2_last,status):
		self.client_code = client_code
		self.name = name
		self.group = group
		self.abn = abn
		self.acn = acn
		self.invoice_email = invoice_email
		self.invoice_type = invoice_type
		self.username = username
		self.password = password
		self.title = title
		self.sms_name = sms_name
		self.contact_2_title = contact_2_title
		self.contact_2_first = contact_2_first
		self.contact_2_last = contact_2_last
		self.status = status


class ClientAddress(db.Model):
	__tablename__ = 'ClientAddress'
	id = db.Column(db.Integer, primary_key=True)
	abn = db.Column(db.String(250))
	suburb = db.Column(db.String(250))
	postcode = db.Column(db.String(30))
	country = db.Column(db.String(50))
	state = db.Column(db.String(50))
	# postal_address = db.Column(db.Boolean, default=False)
	address_1 = db.Column(db.String(250))
	address_2 = db.Column(db.String(250))
	street = db.Column(db.String(100))
	clientinfo_id = db.Column(db.Integer, db.ForeignKey('ClientInfo.id'), nullable=False)

	created_at = db.Column(db.DateTime, nullable=True)

	def __init__(self,abn,suburb,postcode,country,address_1,address_2,street,state,clientinfo_id,):
		self.abn = abn
		self.suburb = suburb
		self.postcode = postcode
		self.country = country
		# self.postal_address = postal_address
		self.address_1 = address_1
		self.address_2 = address_2
		self.street = street
		self.state = state
		self.clientinfo_id = clientinfo_id



class ClientPropInfo(db.Model):
	__tablename__ = 'ClientPropInfo'
	id = db.Column(db.Integer, primary_key=True)
	clientinfo_id = db.Column(db.Integer, db.ForeignKey('ClientInfo.id'), nullable=False)
	# client_code = db.Column(db.String(20), unique=True)
	login_type = db.Column(db.String(150))
	num_props = db.Column(db.String(15))
	# web_pro_free_option = db.Column(db.Boolean, default=False, nullable=False)
	platinum_partner = db.Column(db.String(10))
	title = db.Column(db.String(100))
	first_name = db.Column(db.String(100))
	last_name = db.Column(db.String(100))
	email = db.Column(db.String(100))
	phone_no = db.Column(db.String(15))
	fax_no = db.Column(db.String(50))
	mobile_no = db.Column(db.String(15))
	arrears_phone = db.Column(db.String(15))
	invoice_contact = db.Column(db.String(15))
	props_as_at = db.Column(db.DateTime)
	cnote_year = db.Column(db.String(15))
	

	created_at = db.Column(db.DateTime)

	def __init__(self,clientinfo_id,login_type,num_props,platinum_partner,title,first_name,last_name,email,phone_no,fax_no,mobile_no,arrears_phone,invoice_contact,props_as_at,cnote_year):
		# self.client_code = client_code
		self.clientinfo_id = clientinfo_id
		self.login_type = login_type
		self.num_props = num_props
		self.platinum_partner = platinum_partner
		self.title = title
		self.first_name = first_name
		self.last_name = last_name
		self.email = email
		self.phone_no = phone_no
		self.fax_no = fax_no
		self.mobile_no = mobile_no
		self.arrears_phone = arrears_phone
		self.invoice_contact = invoice_contact
		self.props_as_at = props_as_at
		self.cnote_year = cnote_year
		





class PrincipleInfo(db.Model):
	__tablename__ = 'PrincipleInfo'
	id = db.Column(db.Integer, primary_key=True)
	clientinfo_id = db.Column(db.Integer, db.ForeignKey('ClientInfo.id'), nullable=False)
	client_code = db.Column(db.String(20), unique=True)
	title = db.Column(db.String(100))
	first_name = db.Column(db.String(100))
	last_name = db.Column(db.String(100))
	email = db.Column(db.String(100))
	phone_no = db.Column(db.String(15))
	fax_no = db.Column(db.String(20))
	mobile_no = db.Column(db.String(15))
	c_title = db.Column(db.String(100))

	created_at = db.Column(db.DateTime, nullable=True)

	def __init__(self,clientinfo_id,client_code,title,first_name,last_name,email,phone_no,fax_no,mobile_no,c_title):
		self.clientinfo_id = clientinfo_id
		self.client_code = client_code
		self.title = title
		self.first_name = first_name
		self.last_name = last_name
		self.email = email
		self.phone_no = phone_no
		self.fax_no = fax_no
		self.mobile_no = mobile_no
		self.c_title = c_title
		


class Subscription(db.Model):
	__tablename__ = 'Subscription'
	id = db.Column(db.Integer, primary_key=True)
	clientinfo_id = db.Column(db.Integer, db.ForeignKey('ClientInfo.id'))
	# client_code = db.Column(db.String(20), unique=True)
	renewal_date = db.Column(db.DateTime)
	# hold_payment = db.Column(db.Boolean, default=False)
	referal = db.Column(db.String(10))
	do_not_lapse = db.Column(db.String(10))
	category = db.Column(db.String(50), nullable=False)
	# paying_monthly = db.Column(db.Boolean, default=False)

	created_at = db.Column(db.DateTime, nullable=True)
			
	def __init__(self,clientinfo_id,client_code,renewal_date,referal,do_not_lapse,category):
		self.clientinfo_id = clientinfo_id
		self.client_code = client_code
		self.renewal_date = renewal_date
		self.referal = referal
		self.do_not_lapse = do_not_lapse
		self.category = category

class ClientDiffEmails(db.Model):
	__tablename__ = 'ClientDiffEmails'
	id = db.Column(db.Integer, primary_key=True)
	clientinfo_id = db.Column(db.Integer, db.ForeignKey('ClientInfo.id'))
	email = db.Column(db.String(100))
	accounts_email = db.Column(db.String(100))
	pdf_email = db.Column(db.String(100))
	bdm_email = db.Column(db.String(100))

	def __init__(self,clientinfo_id,email,accounts_email,pdf_email,bdm_email):
		self.clientinfo_id = clientinfo_id
		self.email = email
		self.accounts_email = accounts_email
		self.pdf_email = pdf_email
		self.bdm_email = bdm_email



		
		
		






class Test(db.Model):
	__tablename__ = 'Test'
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(15), nullable=False)
	email = db.Column(db.String(15), nullable=False)

	def __init__(self,name,email):
		self.name = name
		self.email = email




class DebatorInfo(db.Model):
	__tablename__ = 'DebatorInfo'
	id = db.Column(db.Integer, primary_key=True)
	tenant_id_code = db.Column(db.String(20))
	name = db.Column(db.String(100))
	email = db.Column(db.String(100))
	sex = db.Column(db.String(100))
	home_phone = db.Column(db.String(100))
	mobile = db.Column(db.String(100))
	other_contact = db.Column(db.String(100))
	work = db.Column(db.String(100))
	work_address = db.Column(db.String(100))
	managing_agent = db.Column(db.String(100))
	description = db.Column(db.String(100))
	amnt_of_def = db.Column(db.Integer)
	dob = db.Column(db.DateTime)
	date_of_entry = db.Column(db.DateTime)
	service_address = db.Column(db.String(100))
	drive_licence_no = db.Column(db.String(100))
	car_registration_no = db.Column(db.String(100))
	passport = db.Column(db.String(100))
	eviction = db.Column(db.Boolean, default=False)
	eviction_start_date = db.Column(db.DateTime)
	with_hold_data = db.Column(db.Boolean, default=False)
	contact_1_first = db.Column(db.String(100))
	contact_1_last = db.Column(db.String(100)) 


	def __init__(self,tenant_id_code,name,email,sex,home_phone,mobile,other_contact,work,work_address,managing_agent,description,amnt_of_def,dob,date_of_entry,service_address,drive_licence_no,car_registration_no,passport,eviction,eviction_start_date,with_hold_data,contact_1_first,contact_1_last):
		self.tenant_id_code = tenant_id_code
		self.name = name
		self.email = email
		self.sex = sex
		self.home_phone = home_phone
		self.mobile = mobile
		self.other_contact = other_contact
		self.work = work
		self.work_address = work_address
		self.managing_agent = managing_agent
		self.description = description
		self.amnt_of_def = amnt_of_def
		self.dob = dob
		self.date_of_entry = date_of_entry
		self.service_address = service_address
		self.drive_licence_no = drive_licence_no
		self.car_registration_no = car_registration_no
		self.passport = passport
		self.eviction = eviction
		self.eviction_start_date = eviction_start_date
		self.with_hold_data = with_hold_data
		self.contact_1_first = contact_1_first
		self.contact_1_last = contact_1_last













	



