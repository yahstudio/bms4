from flask import Flask ,render_template, request
from models import db
from models import Test, PrincipleInfo ,ClientInfo ,ClientAddress ,ClientPropInfo , Subscription ,DebatorInfo ,ClientDiffEmails
from datetime import datetime
from flask import jsonify
from flask_cors import CORS


app = Flask(__name__)
CORS(app)

# test commit
# ...app config...
db.init_app(app)


app.config['DEBUG'] = True

POSTGRES = {
'user': 'postgres',
'pw': '$ecurity#2018',
'db': 'bms4db',
'host': '159.65.139.47',
'port': '5432',
}

DB_URL = 'postgresql+psycopg2://postgres:$ecurity#2018@localhost:5432/bms4db'


app.config['SQLALCHEMY_DATABASE_URI'] = DB_URL
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False


@app.route('/client', methods=['POST','GET'])
def addclient():
	print("inside")
	errorlist=[]
	try:
		if request.method == 'POST':
			# print('request.data : ',request.data)
			try:
				client_code = request.form['client_code']
				print("client_code",client_code)
				name = request.form['name']
				print("name",name)

				group = request.form['real_group']
				print("group",group)
				abn = request.form['abn']
				print("abn",abn)
				acn = request.form['acn']
				print("acn",acn)
				invoice_email = request.form['invoice_email']
				print("invoice_email",invoice_email)
				invoice_type = request.form['invoice_type']
				print("invoice_type",invoice_type)
				username = request.form['user_name']
				print("username",username)
				password = request.form['password']
				print("password",password)
				title = request.form['title_note']
				print("title",title)
				sms_name = request.form['sms_name']
				print("sms_name",sms_name)
				contact_2_title = request.form['contact_2_title']
				print("contact_2_title",contact_2_title)
				contact_2_first = request.form['contact_2_first']
				contact_2_last = request.form['contact_2_last']
				status = request.form['status']

				user = ClientInfo.query.filter_by(client_code=client_code).first()
				if user:
					print('\n\n\n\n')
					print("in ClientInfo")
					print(name)
					user_id = user.id
					# post = db.session.query(Post).filter(Post.id==id).first()
					change = ClientInfo.query.filter_by(id=user.id)
					change.update(dict(
						name = name,
						group = group,
						abn = abn,
						invoice_email = invoice_email,
						username = username,
						password = password,
						title = title,
						sms_name = sms_name,
						contact_2_title = contact_2_title,
						contact_2_first = contact_2_first,
						contact_2_last = contact_2_last,
						status = status
						))

					db.session.commit()
					client_id = user_id
					print('CLientInfo updated.')
					# return jsonify({"status":"1","message":"Updated Successfully." })
				else:
					Clientinfo = ClientInfo(client_code,name,group,abn,acn,invoice_email,invoice_type,username,password,title,sms_name,contact_2_title,contact_2_first,contact_2_last,status)
					db.session.add(Clientinfo)
					db.session.commit()
					print(Clientinfo.id)
					client_id = Clientinfo.id
					
					print('ClinetInfo saved.')

					# return jsonify({"status":"1","message":"Data Saved Successfully." })



			except Exception as e:
				print('error is', e)
				errorlist.append(e)
				pass

			
			try:
				abn = request.form['abn']
				suburb = request.form['suburb']
				postcode = request.form['post_code']
				country = request.form['country']
				# postal_address = request.form['postal_address_1"']
				address_1 = request.form['address_1']
				address_2 =  request.form['address_2']
				street = request.form['street']
				state = request.form['state']
				clientinfo_id = client_id

				user = ClientAddress.query.filter_by(clientinfo_id=clientinfo_id).first()
				if user:
					print('\n\n\n\n')
					print("in ClientAddress")
					
					user_id = user.id
					# post = db.session.query(Post).filter(Post.id==id).first()
					change = ClientAddress.query.filter_by(id=user.id)
					change.update(dict(
						abn = abn,
						suburb = suburb,
						postcode = postcode,
						country = country,
						address_1 = address_1,
						address_2 = address_2,
						street = street,
						state = state
						))

					db.session.commit()
					
					print('ClientAddress updated.')

				else:
					Client_Address = ClientAddress(abn,suburb,postcode,country,address_1,address_2,street,state,clientinfo_id)
					db.session.add(Client_Address)
					db.session.commit()
					print('ClientAddress saved.')




			except Exception as e:
				print('error is', e)
				errorlist.append(e)
				pass

			
			try:
				clientinfo_id = client_id
				print("clientinfo_id",clientinfo_id)
				login_type = request.form['login_type']
				print("login_type",login_type)
				num_props = request.form['num_props']
				print("num_props",num_props)
				# web_pro_free_option = request.form[''] #empty
				platinum_partner = request.form['platinum_partner']
				print("platinum_partner",platinum_partner)
				if platinum_partner == "1":
					print("inn")
					platinum_partner = True
				if platinum_partner == "0":
					platinum_partner = False
				title = request.form['prop_man_title']
				print("title",title)
				first_name = request.form['prop_man_first']
				print("first_name",first_name)
				last_name = request.form['prop_man_last']
				print("last_name",last_name)
				email = request.form['prop_man_email']
				print("email",email)
				phone_no = request.form['phone_2']
				print("phone_no",phone_no)
				fax_no = request.form['fax']
				print("fax_no",fax_no)
				mobile_no = request.form['mobile_2']
				print("mobile_no",mobile_no)
				arrears_phone = request.form['arrears_phone']
				print("arrears_phone",arrears_phone)
				invoice_contact = request.form['invoice_contact']
				print("invoice_contact",invoice_contact)
				props_as_at1 = request.form['props_as_at']
				if props_as_at1 == '':
					props_as_at1 = "0001-01-01"
					print('props_as_at',props_as_at1)
					props_as_at = datetime.strptime(props_as_at1, '%Y-%m-%d')
					print("props_as_at",props_as_at)
				else:
					props_as_at = datetime.strptime(props_as_at1, '%Y-%m-%d')
				# if props_as_at1 == '':
				# 	print("i none")
				# 	props_as_at1 = None
				# 	print("props_as_at",props_as_at1)
				# 	print("type",type(props_as_at1))
				# else:
				# 	print("props_as_at",props_as_at1)
				# 	print("type",type(props_as_at1))
				# 	props_as_at = datetime.strptime(props_as_at1, '%Y-%m-%d')
				# 	print(type(props_as_at))
				cnote_year = request.form['cnote_year']
				print("cnote_year",cnote_year)


				user = ClientPropInfo.query.filter_by(clientinfo_id=clientinfo_id).first()
				if user:
					print('\n\n\n\n')
					print("in ClientPropInfo")
					
					user_id = user.id
					# post = db.session.query(Post).filter(Post.id==id).first()
					change = ClientPropInfo.query.filter_by(id=user.id)
					change.update(dict(
						login_type = login_type,
						num_props = num_props,
						platinum_partner = platinum_partner,
						title = title,
						first_name = first_name,
						last_name = last_name,
						email = email,
						phone_no = phone_no,
						fax_no = fax_no,
						mobile_no = mobile_no,
						arrears_phone = arrears_phone,
						invoice_contact = invoice_contact,
						props_as_at = props_as_at.date(),
						cnote_year = cnote_year
						))

					db.session.commit()
					
					print('ClientPropInfo updated.')

				else:
					Client_PropInfo = ClientPropInfo(clientinfo_id,login_type,num_props,platinum_partner,title,first_name,last_name,email,phone_no,fax_no,mobile_no,arrears_phone,invoice_contact,props_as_at.date(),cnote_year)
					db.session.add(Client_PropInfo)
					db.session.commit()
					print('ClientPropInfo saved.')

				
			except Exception as e:
				print('error is', e)
				errorlist.append(e)
				pass
			
			

			try:
				clientinfo_id = client_id
				title = request.form['contact_1_title']
				first_name = request.form['contact_1_first']
				last_name = request.form['contact_1_last']
				email = request.form['principal_email']
				phone_no = request.form['phone_1']
				mobile_no = request.form['mobile']
				c_title = request.form['c_title']


				user = PrincipleInfo.query.filter_by(clientinfo_id=clientinfo_id).first()
				if user:
					print('\n\n\n\n')
					print("in PrincipleInfo")
					
					user_id = user.id
					# post = db.session.query(Post).filter(Post.id==id).first()
					change = PrincipleInfo.query.filter_by(id=user.id)
					change.update(dict(
						title = title,
						first_name = first_name,
						last_name = last_name,
						email = email,
						phone_no = phone_no,
						mobile_no = mobile_no,
						c_title = c_title
						))

					db.session.commit()
					
					print('PrincipleInfo updated.')
				else:
					Principle_Info = PrincipleInfo(clientinfo_id,client_code,title,first_name,last_name,email,phone_no,fax_no,mobile_no,c_title)
					db.session.add(Principle_Info)
					db.session.commit()
					print('PrincipleInfo saved')


				
			except Exception as e:
				print('error is', e)
				errorlist.append(e)
				pass

			


			try:
				clientinfo_id = client_id
				email = request.form['email']
				accounts_email = request.form['accounts_email']
				pdf_email = request.form['pdf_email']
				bdm_email = request.form['bdm_email']

				user = ClientDiffEmails.query.filter_by(clientinfo_id=clientinfo_id).first()
				if user:
					print('\n\n\n\n')
					print("in ClientDiffEmails")
					
					user_id = user.id
					# post = db.session.query(Post).filter(Post.id==id).first()
					change = ClientDiffEmails.query.filter_by(id=user.id)
					change.update(dict(
						email = email,
						accounts_email = accounts_email,
						pdf_email = pdf_email,
						bdm_email = bdm_email
						))

					db.session.commit()
					
					print('ClientDiffEmails updated.')
				else:
					ClientDiff_Emails = ClientDiffEmails(clientinfo_id,email,accounts_email,pdf_email,bdm_email)
					db.session.add(ClientDiff_Emails)
					db.session.commit()
					print('ClientDiffEmails saved.')

				
				
			except Exception as e:
				print('error is', e)
				errorlist.append(e)
				pass


			






			try:
				clientinfo_id = client_id
				renewal_date1 = request.form['renewal_date']
				
				if renewal_date1 == '':
					renewal_date1 = "0001-01-01"
					print('renewal_date',renewal_date1)
					renewal_date = datetime.strptime(renewal_date1, '%Y-%m-%d')
					print("renewal_date",renewal_date)
				else:
					renewal_date = datetime.strptime(renewal_date1, '%Y-%m-%d')
				# if renewal_date1 =='':
				# 	renewal_date1 = None

				# 	print("renewal_date1",renewal_date1)
				# 	print("type",type(renewal_date1))
				# else:
				# 	print("renewal_date1",renewal_date1)
				# 	print("type",type(renewal_date1))

				# 	renewal_date = datetime.strptime(renewal_date1, '%Y-%m-%d')
				# 	print(type(renewal_date))
				# if platinum_partner == "on":
				# 	print("inn")
				# 	platinum_partner = True
				# if platinum_partner == "0":
				# 	platinum_partner = False
				# hold_payment = request.form[''] #empty
				referal = request.form['referral']
				do_not_lapse = request.form['no_lapse']
				# paying_monthly = request.form[''] #empty
				category = request.form['category']


				user = Subscription.query.filter_by(clientinfo_id=clientinfo_id).first()
				if user:
					print('\n\n\n\n')
					print("in Subscription")
					
					user_id = user.id
					# post = db.session.query(Post).filter(Post.id==id).first()
					change = Subscription.query.filter_by(id=user.id)
					change.update(dict(
						renewal_date = renewal_date.date(),
						referal = referal,
						do_not_lapse = do_not_lapse,
						category = category



						
						))

					db.session.commit()
					
					print('Subscription updated.')

				else:
					Subscriptions = Subscription(clientinfo_id,client_code,renewal_date.date(),referal,do_not_lapse,category)
					db.session.add(Subscriptions)
					db.session.commit()
					print('Subscription saved.')


			except Exception as e:
				print('error is', e)
				pass

			
			print("done")
			return jsonify({"status":"1" , "message":"Saved Successfully"})
			
	except Exception as e:
		print('\n\n\n\n')
		print('error is : ',e)
		return jsonify({"status":"0" , "message":"Something went wrong."})
		




	try:
		if request.method == 'GET':
			history_data=[]
			
			client_code = request.args.get('client_id')
			
			print(client_code)
			print(type(client_code))
			print("in GET")

			# course = request.form.get('course')
			# print('9999999999999')
			# # myCursor2 = POSTGRES.DebatorInfo()
			# peter = DebatorInfo.query.filter_by(tenant_id_code=tenant_id_code).first()

			# query2 = ("SELECT * FROM DebatorInfo where tenant_id_code = '" + tenant_id_code +  "';")
			# print("innn")
			# myresult2 = myCursor2.execute(query2)
			# return jsonify(myresult2)
			print('\n\n\n\n\n')
			print(client_code)
			user = ClientInfo.query.filter_by(client_code=client_code)
			user_info = user.all()
			
			print(user.all())
			
			if user_info:
				print("in user")
				for i in user:
					print("in in user")
					data={}
					data["client_code_1st"]= i.client_code
					data["status_last"] = i.status
				
					
					clientinfo_id = i.id
					user1 = ClientAddress.query.filter_by(clientinfo_id=clientinfo_id)
					user_info1 = user1.all()
					if user_info1:
						for j in user1:
							data["abn_1st"]=j.abn
							data["state_lst"]=j.state

						user2 = ClientPropInfo.query.filter_by(clientinfo_id=clientinfo_id)
						user_info2 = user2.all()
						if user_info2:
							for k in user2:
								data["login_type"]=k.login_type
								data["mobile_no"]=k.mobile_no

							user3 = PrincipleInfo.query.filter_by(clientinfo_id=clientinfo_id)
							user_info3 = user3.all()
							if user_info3:
								for l in user3:
									data["title"]=l.title
									data["c_title"]=l.c_title
								user4 = ClientDiffEmails.query.filter_by(clientinfo_id=clientinfo_id)
								user_info4 = user4.all()
								if user_info4:
									for m in user4:
										data["email"]=m.email
										data["bdm_email"]=m.bdm_email
									user5 = Subscription.query.filter_by(clientinfo_id=clientinfo_id)
									user_info5 = user5.all()
									if user_info5:
										for n in user5:
											data["renewal_date"]=n.renewal_date
											data["category"]=n.category


						history_data.append(data)


								





		



				return jsonify({'data': data ,"status":"1" , "message":"data found"})
			else:
				print("NOT")
				return jsonify({"status":"0" , "message":"data not found"})


	except Exception as e:
		print("GET error is",e)
		return jsonify({"status":"0" , "message":"Something went wrong."})
		














































@app.route('/debator', methods=['POST','GET'])
# @cross_origin()
def adddebator():
	print("inside")
	
	try:
		if request.method == 'POST':
			try:
				
				
				print('in POST')
				tenant_id_code = request.form['client_id']
				print("tenant_id_code",tenant_id_code)
				name = request.form['name']
				print("name",name)
				email = request.form['email']
				print("email",email)
				sex = request.form['sex']
				print("sex",sex)
				home_phone = request.form['phone_1']
				print("home_phone",home_phone)
				mobile = request.form['mobile']
				print("mobile",mobile)
				other_contact = request.form['other_contact']
				print("other_contact",other_contact)
				work = request.form['work']
				print("work",work)
				work_address = request.form['WORKADDRESS']
				managing_agent = request.form['managing_agent']
				description = request.form['description']
				amnt_of_def = request.form['AMNTOFDEF']
				dob1 = request.form['dob']
				# dob1 = "12-01-1994"
				# adstr=" 00:00:00"
				# dob2 = str(dob1) + str(adstr)
				# print(dob2)
				dob = datetime.strptime(dob1, '%Y-%m-%d')
				print(type(dob))
				date_of_entry1 = request.form['date_of_entry']
				# date_of_entry1 = "11-06-2014"
				# date_of_entry2 =  str(date_of_entry1) + str(adstr)
				# date_of_entry = datetime.strptime(date_of_entry1, '%Y-%m-%d %H:%M:%S')
				date_of_entry = datetime.strptime(date_of_entry1, '%Y-%m-%d')

				print(type(date_of_entry))
				print(date_of_entry)

				service_address = request.form['service_address']
				drive_licence_no = request.form['drive_lic_num']
				car_registration_no = request.form['car_rego']
				passport = request.form['passport']
				eviction = request.form['eviction']
				if eviction == "0":
					eviction = False
				if eviction == "1":
					print("in")
					eviction = True
				print("eviction",eviction,type(eviction))
				eviction_start_date1 = request.form['EvictionStartDate']
				if eviction_start_date1 == '':
					eviction_start_date1 = "0001-01-01"
					print('eviction_start_date',eviction_start_date1)
					eviction_start_date = datetime.strptime(eviction_start_date1, '%Y-%m-%d')
					print("eviction_start_date",eviction_start_date)
				else:
					eviction_start_date = datetime.strptime(eviction_start_date1, '%Y-%m-%d')


				print(type(eviction_start_date))
				# if eviction_start_date == "0000-00-00":

				with_hold_data = request.form['WithholdData']
				if with_hold_data == "1":
					print("inn")
					with_hold_data = True
				if with_hold_data == "0":
					with_hold_data = False

				contact_1_first = request.form['contact_1_first']
				contact_1_last = request.form['contact_1_last']

				
				user = DebatorInfo.query.filter_by(tenant_id_code=tenant_id_code).first()
				print("name" ,name)
				if user:
					print('\n\n\n\n')
					print("in DebatorInfo")
					print(name)
					user_id = user.id
					# post = db.session.query(Post).filter(Post.id==id).first()
					change = DebatorInfo.query.filter_by(id=user.id)
					# print("name",change.data)
					
					# change.name = "TARUN"
					
					
					print("found")
					print("name:"  ,name)

					change.update(dict(
						name = name,
						email = email,
						sex = sex,
						home_phone = home_phone,
						mobile = mobile,
						other_contact = other_contact,
						work = work,
						work_address = work_address,
						managing_agent = managing_agent,
						description = description,
						amnt_of_def = amnt_of_def,
						dob = dob.date(),
						date_of_entry = date_of_entry.date(),
						service_address = service_address,
						drive_licence_no = drive_licence_no,
						car_registration_no = car_registration_no,
						passport = passport,
						eviction = eviction,
						eviction_start_date = eviction_start_date.date(),
						with_hold_data = with_hold_data,
						contact_1_first = contact_1_first,
						contact_1_last = contact_1_last

						))
					db.session.commit()
					print("done")
					return jsonify({"status":"1","message":"Saved Successfully." })
				else:
					Debator_Info = DebatorInfo(tenant_id_code,name,email,sex,home_phone,mobile,other_contact,work,work_address,managing_agent,description,amnt_of_def,dob.date(),date_of_entry.date(),service_address,drive_licence_no,car_registration_no,passport,eviction,eviction_start_date.date(),with_hold_data,contact_1_first,contact_1_last)
					db.session.add(Debator_Info)
					db.session.commit()
					return jsonify({"status":"1","message":"Saved Successfully." })
			except Exception as e:
				print('error is : ',e)
				return jsonify({"status":"0","message":"Something went wrong." })

			


		
			# return('It is updated.')

	except Exception as e:
		print('\n\n\n\n')
		print('error is : ',e)
		return jsonify({"status":"0","message":"Something went wrong." })
		# return('Something is broken.')


	try:
		if request.method == 'GET':
			history_data=[]
			
			tenant_id_code = request.args.get('client_id')
			
			print(tenant_id_code)
			print(type(tenant_id_code))
			print("in GET")

			# course = request.form.get('course')
			# print('9999999999999')
			# # myCursor2 = POSTGRES.DebatorInfo()
			# peter = DebatorInfo.query.filter_by(tenant_id_code=tenant_id_code).first()

			# query2 = ("SELECT * FROM DebatorInfo where tenant_id_code = '" + tenant_id_code +  "';")
			# print("innn")
			# myresult2 = myCursor2.execute(query2)
			# return jsonify(myresult2)
			print('\n\n\n\n\n')
			print(tenant_id_code)
			user = DebatorInfo.query.filter_by(tenant_id_code=tenant_id_code)
			user_info = user.all()
			
			print(user.all())
			
			if user_info:
				print("in user")
				for i in user:
					print("in in user")
					data={}
					data["tenant_id_code"]= i.tenant_id_code
					data["name"] = i.name
					data["email"] = i.email
					data["sex"] = i.sex
					data["home_phone"] = i.home_phone
					data["mobile"] = i.mobile
					data["other_contact"] = i.other_contact
					data["work"] = i.work
					data["work_address"] = i.work_address
					data["managing_agent"] = i.managing_agent
					data["description"] = i.description
					data["amnt_of_def"] = i.amnt_of_def
					data["dob"] = i.dob
					data["date_of_entry"] = i.date_of_entry
					data["service_address"] = i.service_address
					data["drive_licence_no"] = i.drive_licence_no
					data["car_registration_no"] = i.car_registration_no
					data["passport"] = i.passport
					data["eviction"] = i.eviction
					data["eviction_start_date"] = i.eviction_start_date
					data["with_hold_data"] = i.with_hold_data
					history_data.append(data)


				print("data",data)



				return jsonify({'data': data ,"status":"1" , "message":"data found"})
			else:
				print("NOT")
				return jsonify({"status":"0" , "message":"data not found"})


	except Exception as e:
		print("GET error is",e)
		return('Something is broken.')
   









@app.route("/")
def hello():
	try:
		db.session.query("1").from_statement(text("SELECT 1")).all()
		return('It works.')
	except Exception as e:
		error=[]
		e1={}
		print('\n\n\n\n')
		print('error is : ',e)
		e1['error']=e
		error.append(e1)
		# return('Something is broken.')
		return jsonify(error=error)

	# return("Hello World!")
 
if __name__ == "__main__":
	app.secret_key = 'asdf8sdufsd8f73jhnr7dyfsdfish%^$dfds'

	app.run('159.65.139.47',8899)



# http://159.65.139.47:8899/client
# http://159.65.139.47:8899/debator



# return jsonify(
#         username=g.user.username,
#         email=g.user.email,
#         id=g.user.id
#     )


# return jsonify({"status":"ok", 'content':content})

