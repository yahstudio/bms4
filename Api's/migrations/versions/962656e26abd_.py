"""empty message

Revision ID: 962656e26abd
Revises: 4fba20d369f7
Create Date: 2019-02-08 13:36:10.503577

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '962656e26abd'
down_revision = '4fba20d369f7'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('ClientPropInfo', 'props_as_at')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('ClientPropInfo', sa.Column('props_as_at', postgresql.TIMESTAMP(), autoincrement=False, nullable=True))
    # ### end Alembic commands ###
