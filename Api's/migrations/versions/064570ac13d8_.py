"""empty message

Revision ID: 064570ac13d8
Revises: 2a3bc3a23c41
Create Date: 2019-02-07 10:02:08.231763

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '064570ac13d8'
down_revision = '2a3bc3a23c41'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('Subscription', sa.Column('category', sa.String(length=50), nullable=False))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('Subscription', 'category')
    # ### end Alembic commands ###
